package com.example.asus.tipcalculator;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {

    public EditText bill;
    public EditText people;
    public SeekBar seekbar;
    public TextView tip;
    public TextView total;
    private TextView seekbarValue;
    private TextView seek;
     String billInp;
     String numOfPeople;
    Double perc;
    Double per;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        bill = (EditText) findViewById(R.id.bill);
        people = (EditText) findViewById(R.id.numberOfPeople);
        seekbar = (SeekBar) findViewById(R.id.seekBar);
        tip = (TextView) findViewById(R.id.tipPerPerson);
        total = (TextView) findViewById(R.id.totalPerPerson);
        seek = (TextView) findViewById(R.id.seek);

        billInp = bill.getText().toString();
        numOfPeople = people.getText().toString();


          seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {


                @Override
                public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                    perc = (double)i;
                  seek.setText(String.valueOf(i)+"%");
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {

                    if(bill.getText().toString()    ==  "" && people.getText().toString()   == ""){
                        total.setText("Php");
                        tip.setText("Php");
                    }
                    else
                        calc();


                }
            });//end of seekbar

            bill.addTextChangedListener((new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    if(bill.getText().toString()    ==  "" && people.getText().toString()   == ""){
                        total.setText("Php");
                        tip.setText("Php");
                    }
                    else
                        calc();
                }
            }));

        people.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                if(bill.getText().toString()    ==  "" || people.getText().toString()   == ""){
                    total.setText("Php");
                    tip.setText("Php");
                }
                else
                calc();

            }
        });






    }//end of on create

    public void calc(){
        try {


            if(bill.getText().toString()    ==  "" && people.getText().toString()   == "") {

                total.setText("Php");
                tip.setText("Php");

            }
            else {
                Double billy, totalTip, tipPerPerson;
                int b;
                int numofPeople;

                Double pes;
                numofPeople = Integer.parseInt(people.getText().toString());
                billy = Double.parseDouble(bill.getText().toString());
                pes = perc / 100;
                tipPerPerson = (billy / numofPeople) * pes;

                totalTip = (billy / numofPeople) + tipPerPerson;
                total.setText("Php" + totalTip);
                tip.setText("Php" + tipPerPerson);

            }
        }
        catch(Exception s){
            total.setText("Php");
            tip.setText("Php");
        }
    }


        }







